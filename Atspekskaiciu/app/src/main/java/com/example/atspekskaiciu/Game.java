package com.example.atspekskaiciu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class Game extends Activity {

    private int limit_from = 1;
    private int limit_to = 10;
    private int entered_number = 0;
    private int random_number = 0;
    private int current_turn = 0;
    private int turns = 10;

    private String name = "";

    ArrayList<LeaderboardEntry>guess_results;

    LeaderboardAdapter2 mAdapter;

    ListView mListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SharedPreferences sharedPref = getSharedPreferences("Nustatymai", Context.MODE_PRIVATE);
        name = sharedPref.getString("name", "");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        updateFields();

        Random ran = new Random();
        random_number = ran.nextInt(limit_to) + limit_from;

        guess_results = new ArrayList<>();
        //guess_results.add(Integer.toString(entered_number));

        ListView resultListField = findViewById(R.id.result_view);
        mAdapter = new LeaderboardAdapter2(guess_results, this);

       // ArrayAdapter<String> resultsAdapter =
               // new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, guess_results);

        resultListField.setAdapter(mAdapter);
    }

    protected void updateFields()
    {
        TextView turnField2 = findViewById(R.id.textView2);
        SharedPreferences sharedPref = getSharedPreferences("Nustatymai", Context.MODE_PRIVATE);
        name = sharedPref.getString("name", "");

        String turn_string = "Ėjimas: " + Integer.toString(current_turn);
        if(entered_number > random_number && current_turn > 0) {
            String guess_string = "Jūs spėjote: " + Integer.toString(entered_number) + " , skaičius yra mažesnis!";
            turnField2.setText(guess_string);
            guess_results.add(new LeaderboardEntry(0, entered_number, name));
        }
        else if (current_turn > 0){
            String guess_string = "Jūs spėjote: " + Integer.toString(entered_number) + " , skaičius yra didesnis!";
            turnField2.setText(guess_string);
            guess_results.add(new LeaderboardEntry(0, entered_number, name));
        }


        TextView turnField = findViewById(R.id.textView);
        turnField.setText(turn_string);

        ListView resultListField = findViewById(R.id.result_view);
        mAdapter = new LeaderboardAdapter2(guess_results, this);
        resultListField.setAdapter(mAdapter);


        //TextView sk = findViewById(R.id.textView7);
        //sk.setText(Integer.toString(random_number));
        String ribos_string = "Atspekite skaičių intervale nuo " + limit_from + " iki " + limit_to;
        TextView ribos = findViewById(R.id.textView6);
        ribos.setText(ribos_string);

        String liko_string = "Jums liko: " + (turns - current_turn) + " ėjimai";

        TextView liko = findViewById(R.id.textView7);
        liko.setText(liko_string);
    }

    public void onGuessClick(View view)
    {
        current_turn += 1;
        EditText inputField = findViewById((R.id.editText2));
        String inputString = inputField.getText().toString();
        
            entered_number = Integer.parseInt(inputString);
        if (entered_number == random_number) {
                Intent intent = new Intent(this, Win.class);
                Bundle bundle = new Bundle();
                bundle.putInt("ejimas", current_turn);
                bundle.putInt("viso", turns);

                intent.putExtras(bundle);
                startActivity(intent);
                finish();

            } else if (current_turn == turns) {
                Intent intent = new Intent(this, Loose.class);
                Bundle bundle = new Bundle();
                bundle.putInt("viso", turns);

                intent.putExtras(bundle);
                startActivity(intent);
                finish();
            }
            updateFields();
        }
    }
