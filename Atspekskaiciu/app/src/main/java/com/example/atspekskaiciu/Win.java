package com.example.atspekskaiciu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class Win extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_win);

        Bundle bundle;

        bundle = getIntent().getExtras();
        int ejimas = bundle.getInt("ejimas");
        int viso = bundle.getInt("viso");

        TextView veiksmai = findViewById(R.id.veiksmai);
        veiksmai.setText("Buvo " + viso + " ėjimai. Jūs atspėjote per: " + ejimas);

        LeaderboardDatabaseHandler dbhandler = new LeaderboardDatabaseHandler(this);
        SharedPreferences sharedPref = getSharedPreferences("Nustatymai", Context.MODE_PRIVATE);
        String name = sharedPref.getString("name", "");

        dbhandler.addEntry(new LeaderboardEntry(0, ejimas, name));

    }

}