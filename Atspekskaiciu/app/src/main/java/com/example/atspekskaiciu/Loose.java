package com.example.atspekskaiciu;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Loose extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loose);

        Bundle bundle;

        bundle = getIntent().getExtras();
        int ejimas = bundle.getInt("ejimas");
        int viso = bundle.getInt("viso");

        TextView veiksmai = findViewById(R.id.textView8);
        veiksmai.setText("Buvo " + viso + " ėjimai.");
    }
}
