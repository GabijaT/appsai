package com.example.atspekskaiciu;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Settings extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        loadSettings();
    }
    public void loadSettings()
    {
        SharedPreferences sharedPref = getSharedPreferences("Nustatymai", Context.MODE_PRIVATE);
        String name = sharedPref.getString("name", "");
        Integer age = sharedPref.getInt("age", 0);

        EditText nameField = findViewById(R.id.NameS);
        nameField.setText(name);

        EditText ageField = findViewById(R.id.AgeS);
        ageField.setText(age+"");
    }

    public void mySaveClick(View view)
    {
        EditText nameField = findViewById(R.id.NameS);
        String name = nameField.getText().toString();

        EditText ageField = findViewById(R.id.AgeS);
        Integer age = Integer.parseInt(ageField.getText().toString());

        SharedPreferences.Editor sharedPreEditor = getSharedPreferences("Nustatymai", Context.MODE_PRIVATE).edit();
        sharedPreEditor.putString("name", name);
        sharedPreEditor.putInt("age", age);
        sharedPreEditor.apply();

        finish();

    }
}

