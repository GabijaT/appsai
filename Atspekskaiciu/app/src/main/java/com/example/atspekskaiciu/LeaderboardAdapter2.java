package com.example.atspekskaiciu;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class LeaderboardAdapter2 extends BaseAdapter {

    ArrayList<LeaderboardEntry> dataList;
    Activity activity;

    LeaderboardAdapter2(ArrayList<LeaderboardEntry> d, Activity a)
    {
        dataList = d;
        activity = a;
    }

    public int getCount()
    {
        if (dataList != null)
        {
            return dataList.size();
        }
        return 0;
    }

    public long getItemId(int position) { return position; }

    public Object getItem(int position)
    {
        if(dataList != null) {
            return dataList.get(position);
        }
        return null;
    }

    public View getView(int position, View convertView, ViewGroup viewGroup)
    {
        View vi = convertView;
        if(vi == null)
        {
            LayoutInflater li = LayoutInflater.from(activity);
            vi = li.inflate(R.layout.leaderboard_list_layout2, null);
        }

        TextView currentText = (TextView)vi.findViewById(R.id.listview_current_turn);
        TextView nameText = (TextView)vi.findViewById(R.id.listview_name);

        LeaderboardEntry le = dataList.get(position);

        currentText.setText(Integer.toString(le.getCurrent_turn()));
        nameText.setText(le.getName());

        return vi;
    }
}

