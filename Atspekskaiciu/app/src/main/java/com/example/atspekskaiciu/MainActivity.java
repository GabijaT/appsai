package com.example.atspekskaiciu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_linear);
    }

    public void on_about_click(View view)
    {
        Intent intent = new Intent(this, About.class);
        startActivity(intent);
    }

    public void on_settings_click(View view)
    {
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    public void on_play_click(View view)
    {
        Intent intent = new Intent(this, Game.class);
        startActivity(intent);
    }
    public void on_leaderboard_click(View view)
    {
        Intent intent = new Intent(this, LeaderboardActivity.class);
        startActivity(intent);
    }
}
