package com.example.atspekskaiciu;

public class LeaderboardEntry {

    private int ID;
    private int current_turn;
    private String name;


    public LeaderboardEntry()
    {
        ID = 0;
        current_turn = 0;
        name = "";
    }

    public LeaderboardEntry(int ID, int current_turn, String name)
    {
        this.ID = ID;
        this.current_turn = current_turn;
        this.name = name;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public void setCurrent_turn(int val)
    {
        current_turn = val;
    }

    public int getCurrent_turn()
    {
        return current_turn;
    }

    public void setName(String val)
    {
        name = val;
    }

    public String getName() {
        return name;
    }
}